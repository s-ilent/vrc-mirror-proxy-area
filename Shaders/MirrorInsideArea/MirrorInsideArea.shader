// Made with Amplify, edited to do what Amplify can't. 
// -- Silent
Shader "Silent/MirrorInsideArea"
{
	Properties
	{
		[Header(This shader renders the area reflection probe.)]
		[Header(Only visible in the mirror.)]
		[ToggleUI]_Debug("Debug mode (show normally)", Float) = 0
		_Smoothness("Smoothness", Range(0, 1)) = 1.0
		[Space(15)]
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("Cull Mode", Int) = 0
	}
	
	SubShader
	{
		Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True" }
		LOD 100
		Cull [_CullMode]

		
		Pass
		{
			CGPROGRAM
			
			#pragma target 3.0 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_FRAG_WORLD_POSITION

			#pragma multi_compile UNITY_SPECCUBE_BLENDING
			#pragma multi_compile UNITY_SPECCUBE_BOX_PROJECTION

			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 posWS : TEXCOORD1;
				float4 normalWS : TEXCOORD2;
				float4 tangentWS : TEXCOORD3;
				float4 bitangentWS : TEXCOORD4;
			};

			float farDistance7(  )
			{
				//return 1.0e-8f;
				return 1.0e-2f;
			}
			
			float IsInMirror3(  )
			{
				return unity_CameraProjection[2][0] != 0.f || unity_CameraProjection[2][1] != 0.f;
			}
			
			float _Debug;
			float _Smoothness;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				//o.texcoord.xy = v.texcoord.xy;
				//o.texcoord.zw = v.texcoord1.xy;
				
				
				float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.posWS.xyz = ase_worldPos;
				float3 ase_worldNormal = UnityObjectToWorldNormal(v.normal);
				o.normalWS.xyz = ase_worldNormal;
				float3 ase_worldTangent = UnityObjectToWorldDir(v.tangent);
				o.tangentWS.xyz = ase_worldTangent;
				float ase_vertexTangentSign = v.tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.bitangentWS.xyz = ase_worldBitangent;

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.posWS.w = 0;
				o.normalWS.w = 0;
				o.tangentWS.w = 0;
				o.bitangentWS.w = 0;
				o.vertex = UnityObjectToClipPos(v.vertex);
				
				// ase common template code
				float localfarDistance7 = farDistance7();
				#ifdef UNITY_REVERSED_Z
				float staticSwitch5 = localfarDistance7;
				#else
				float staticSwitch5 = ( o.vertex.w - localfarDistance7 );
				#endif
				o.vertex = (float4(o.vertex.x , o.vertex.y , staticSwitch5 , o.vertex.w));
				return o;
			}
			
			fixed4 frag (v2f i, uint facing : SV_IsFrontFace ) : SV_Target
			{
				//float localIsInMirror3 = IsInMirror3();
				float localIsInMirror3 = _Debug? true : IsInMirror3();
				clip(saturate(localIsInMirror3) - 0.5);

				// ase common template code
				float3 ase_worldPos = i.posWS.xyz;
				float3 ase_worldViewDir = UnityWorldSpaceViewDir(ase_worldPos);
				ase_worldViewDir = normalize(ase_worldViewDir);

				float3 ase_worldNormal 		= (i.normalWS.xyz);
				float3 ase_worldTangent 	= (i.tangentWS.xyz);
				float3 ase_worldBitangent 	= (i.bitangentWS.xyz);

				if (!facing) 
				{
					ase_worldNormal *= -1;
					ase_worldTangent *= -1;
					ase_worldBitangent *= -1;
				}

				float3 tanToWorld0 = (float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x ));
				float3 tanToWorld1 = (float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y ));
				float3 tanToWorld2 = (float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z ));

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				float3 normalizedWorldNormal = normalize( ase_worldNormal );

				UnityGIInput data;
				UNITY_INITIALIZE_OUTPUT( UnityGIInput, data );
				data.worldPos = WorldPosition;
				data.worldViewDir = ase_worldViewDir;
				data.probeHDR[0] = unity_SpecCube0_HDR;
				data.probeHDR[1] = unity_SpecCube1_HDR;
				#if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION //specdataif0
				data.boxMin[0] = unity_SpecCube0_BoxMin;
				#endif //specdataif0
				#if UNITY_SPECCUBE_BOX_PROJECTION //specdataif1
				data.boxMax[0] = unity_SpecCube0_BoxMax;
				data.probePosition[0] = unity_SpecCube0_ProbePosition;
				data.boxMax[1] = unity_SpecCube1_BoxMax;
				data.boxMin[1] = unity_SpecCube1_BoxMin;
				data.probePosition[1] = unity_SpecCube1_ProbePosition;
				#endif //specdataif1
				Unity_GlossyEnvironmentData g1 = UnityGlossyEnvironmentSetup( _Smoothness, ase_worldViewDir, reflect( float3( 0,0,0 ) , normalizedWorldNormal ), float3(0,0,0));
				float3 indirectSpecular1 = UnityGI_IndirectSpecular( data, 1.0, reflect( float3( 0,0,0 ) , normalizedWorldNormal ), g1 );

				float4 appendResult11 = (float4(indirectSpecular1 , localIsInMirror3));

				fixed4 myColorVar;
				myColorVar = appendResult11;
				return myColorVar;
			}
			ENDCG
		}
	}	
	
}