# VRC Mirror Proxy Area

A shader that renders the area's reflection probe on the inside, which is only visible through a mirror. This is useful for creating a low-quality proxy background of a space for the mirror reflection.

## Usage

Place the shader into your Assets folder. 

Then create a new material with the shader and a cube without a collider. Place the cube on a reflection only layer, and assign the material to it. The cube will display the local reflection probes inside it, including box projection. The shape and size of the cube affects how the background appears, so check out how it looks from within the mirror. Best used with box projected cubemaps. Note that while the box has visible depth and parallax, it won't clip into objects (unless they're very large) and essentially renders just slightly in front of the skybox.

# License
MIT license.